# fabrick-banking-account

## Info
This project uses Java version 8, it is strongly recommended not to use other versions

## Getting started

git clone https://gitlab.com/develop.kalmykov/fabrick-banking-account.git

mvn clean install -DskipTests

## Test and Deploy

Use the built-in tools in IDEA or Eclipse
