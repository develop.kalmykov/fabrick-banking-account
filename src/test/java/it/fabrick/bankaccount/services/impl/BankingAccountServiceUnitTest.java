package it.fabrick.bankaccount.services.impl;

import it.fabrick.bankaccount.exceptions.FabrickInternalErrorException;
import it.fabrick.bankaccount.exceptions.FabrickNotFoundExceptionException;
import it.fabrick.bankaccount.models.dto.fabrick.account.BankingAccountPayloadDTO;
import it.fabrick.bankaccount.models.dto.fabrick.account.BankingAccountResponseDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.AccountDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.CreditorDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferPayloadDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferRequestDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferResponseDTO;
import it.fabrick.bankaccount.restclient.FabrickRestClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class BankingAccountServiceUnitTest {
    @InjectMocks
    private BankingAccountService bankingAccountService;

    @Mock
    private FabrickRestClient restClient;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @DisplayName("Get transactions list with valid input params")
    void test_GetAccountTransactionsList_callWithValidParams() throws FabrickInternalErrorException {
        LocalDate dateFrom = LocalDate.of(2019, 01, 01);
        LocalDate dateTo = LocalDate.of(2019, 12, 01);

        BankingAccountResponseDTO response = getAccountResponse();

        when(restClient.getAccountTransactionsList(any(), any(), any(), any())).thenReturn(new ResponseEntity<>(response, HttpStatus.OK));

        List<BankingAccountPayloadDTO> transactionList = bankingAccountService.getAccountTransactionsList(dateFrom, dateTo);

        assertFalse(CollectionUtils.isEmpty(transactionList));
    }

    @Test
    @DisplayName("Get transactions list with invalid input params. Throws FabrickInternalErrorException")
    void test_GetAccountTransactionsList_callWithInvalidParams() throws FabrickInternalErrorException {
        LocalDate dateFrom = LocalDate.of(2019, 01, 01);
        LocalDate dateTo = LocalDate.of(2019, 12, 01);

        when(restClient.getAccountTransactionsList(any(), any(), any(), any()))
                .thenThrow(new FabrickInternalErrorException("Errore recupero lista transazioni"));

        assertThrows(FabrickInternalErrorException.class, () -> bankingAccountService.getAccountTransactionsList(dateFrom, dateTo));
    }

    @Test
    @DisplayName("Get account info with valid input params")
    void test_getAccountCredit_withValidAccountId() throws FabrickNotFoundExceptionException {
        Long accountId = 14537780L;
        BankingAccountResponseDTO response = getAccountResponse();
        when(restClient.getAccountCredit(any(), any(), any()))
                .thenReturn(new ResponseEntity<>(response, HttpStatus.OK));

        BankingAccountPayloadDTO accountCredit = bankingAccountService.getAccountCredit(accountId);
        assertFalse(Objects.isNull(accountCredit));
        assertEquals(accountCredit.getAccountId(), "14537780");
    }

    @Test
    @DisplayName("Get account info with invalid input params. Throws FabrickNotFoundExceptionException")
    void test_getAccountCredit_withInvalidAccountId() throws FabrickNotFoundExceptionException {
        Long accountId = 10000000L;
        when(restClient.getAccountCredit(any(), any(), any()))
                .thenThrow(new FabrickNotFoundExceptionException("Acconto non trovato"));

        assertThrows(FabrickNotFoundExceptionException.class, () -> bankingAccountService.getAccountCredit(accountId));
    }

    @Test
    @DisplayName("Make money transfer with valid input params")
    void test_makeMoneyTransfer_withValidInputParams() {
        Long accountId = 14537780L;
        MoneyTransferRequestDTO body = getMoneyTransferBody();
        MoneyTransferResponseDTO response = getMoneyTransferResponseDTO();
        when(restClient.makeMoneyTransfer(any(), any(), any(), any(), any()))
                .thenReturn(new ResponseEntity<>(response, HttpStatus.OK));

        MoneyTransferResponseDTO moneyTransfer = bankingAccountService.makeMoneyTransfer(accountId, body);
        assertNotNull(moneyTransfer.getPayload());
        assertNotNull(moneyTransfer.getStatus());
        assertEquals(moneyTransfer.getStatus(), "OK");
        assertNotNull(moneyTransfer.getPayload().getMoneyTransferId());
    }

    @Test
    @DisplayName("Make money transfer with invalid input params. Throws FabrickInternalErrorException")
    void test_makeMoneyTransfer_withInvalidInputParams() {
        Long accountId = 14537780L;
        when(restClient.makeMoneyTransfer(any(), any(), any(), any(), any()))
                .thenThrow(new FabrickInternalErrorException("Errore invio bonifico"));

        assertThrows(FabrickInternalErrorException.class, () -> bankingAccountService.makeMoneyTransfer(accountId, new MoneyTransferRequestDTO()));
    }

    private MoneyTransferRequestDTO getMoneyTransferBody() {
        MoneyTransferRequestDTO moneyTransferRequestDTO = new MoneyTransferRequestDTO();
        moneyTransferRequestDTO.setCreditor(new CreditorDTO("LUCA TERRIBILE", new AccountDTO("IT75F0300203280479227258345")));
        moneyTransferRequestDTO.setExecutionDate(LocalDate.of(2024, 05, 29));
        moneyTransferRequestDTO.setDescription("Payment invoice 75/2017");
        moneyTransferRequestDTO.setAmount(new BigDecimal(8));
        moneyTransferRequestDTO.setCurrency("EUR");
        return moneyTransferRequestDTO;

    }

    private MoneyTransferResponseDTO getMoneyTransferResponseDTO() {
        MoneyTransferResponseDTO response = new MoneyTransferResponseDTO(new MoneyTransferPayloadDTO("528047241", "8955475180803268", null, "BOOKED", "NOTPROVIDED", "OUTGOING", null, null));
        response.setStatus("OK");
        return response;
    }

    private BankingAccountResponseDTO getAccountResponse() {
        BankingAccountResponseDTO response = new BankingAccountResponseDTO();
        response.setStatus("OK");
        response.setError(new LinkedList<>());
        List<BankingAccountPayloadDTO> list = new LinkedList<>();
        list.add(new BankingAccountPayloadDTO("14537780", "IT40L0326822311052923800661", "03268", "22311", "IT", "40", "L", "52923800661", "Test api", "Conto Websella", "LUCA TERRIBILE", new Date(), "EUR", null));
        BankingAccountPayloadDTO payload = new BankingAccountPayloadDTO("14537780", "IT40L0326822311052923800661", "03268", "22311", "IT", "40", "L", "52923800661", "Test api", "Conto Websella", "LUCA TERRIBILE", new Date(), "EUR", list);
        response.setPayload(payload);
        return response;
    }
}