package it.fabrick.bankaccount.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.fabrick.bankaccount.exceptions.BindingException;
import it.fabrick.bankaccount.exceptions.FabrickNotFoundExceptionException;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.AccountDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.CreditorDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferRequestDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BankingAccountControllerIntegrationTest {
    private final static String BASE_URL = "/api/v1/banking/accounts";

    private static MoneyTransferRequestDTO moneyTransferRequestDTO;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        moneyTransferRequestDTO = new MoneyTransferRequestDTO();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @DisplayName("Retrieve a list of bank account's transactions with no date range")
    void test_getAccountTransactionsList_withoutDateRange() throws Exception {
        final String url = BASE_URL + "/transactions";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @DisplayName("Retrieve bank account by correct account id")
    void test_getAccountCredit_withCorrectInputParams() throws Exception {
        String url = BASE_URL + "/{id}";
        mockMvc.perform(MockMvcRequestBuilders.get(url, 14537780L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.accountId", is("14537780")))
                .andExpect(jsonPath("$.currency", is("EUR")));
    }

    @Test
    @DisplayName("Retrieve bank account by invalid account id")
    void test_getAccountCredit_withInvalidInputParams() throws Exception {
        String url = BASE_URL + "/{id}";
        mockMvc.perform(MockMvcRequestBuilders.get(url, 1453778986L))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof FabrickNotFoundExceptionException));

    }

    @Test
    @DisplayName("Make money transfer with correct input params")
    void test_makeMoneyTransfer_withCorrectInputParams() throws Exception {
        moneyTransferRequestDTO.setCreditor(new CreditorDTO("LUCA TERRIBILE", new AccountDTO("IT75F0300203280479227258345")));
        moneyTransferRequestDTO.setExecutionDate(LocalDate.of(2024, 05,29));
        moneyTransferRequestDTO.setDescription("Payment invoice 75/2017");
        moneyTransferRequestDTO.setAmount(new BigDecimal(8));
        moneyTransferRequestDTO.setCurrency("EUR");
        String url = BASE_URL + "/{id}/money-transfer";
        mockMvc.perform(MockMvcRequestBuilders.post(url, 14537780L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(moneyTransferRequestDTO)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.status", is("OK")));
    }

    @Test
    @DisplayName("Make money transfer with invalid input params")
    void test_makeMoneyTransfer_withInvalidInputParams() throws Exception {
        moneyTransferRequestDTO.setExecutionDate(LocalDate.of(2024, 05,29));
        moneyTransferRequestDTO.setDescription("Payment invoice 75/2017");
        moneyTransferRequestDTO.setAmount(new BigDecimal(8));
        moneyTransferRequestDTO.setCurrency("EUR");
        String url = BASE_URL + "/{id}/money-transfer";
        mockMvc.perform(MockMvcRequestBuilders.post(url, 14537780L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(moneyTransferRequestDTO)))
                .andExpect(status().is4xxClientError())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof BindingException));
    }
}