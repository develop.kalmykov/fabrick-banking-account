package it.fabrick.bankaccount.controllers;

import it.fabrick.bankaccount.exceptions.BindingException;
import it.fabrick.bankaccount.exceptions.FabrickInternalErrorException;
import it.fabrick.bankaccount.exceptions.FabrickNotFoundExceptionException;
import it.fabrick.bankaccount.exceptions.InvalidInputException;
import it.fabrick.bankaccount.models.dto.ErrorResponseDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@ControllerAdvice
@RestController
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final String DEFAULT_ERROR_MESSAGE = "Unknown generic error";
    private static final int DEFAULT_ERROR_CODE = 500;

    @ExceptionHandler(FabrickNotFoundExceptionException.class)
    public final ResponseEntity<ErrorResponseDTO> handleException(FabrickNotFoundExceptionException e) {
        List<String> errorsList =  new LinkedList<>();
        errorsList.add(e.getMessage());
        return new ResponseEntity<>(new ErrorResponseDTO(LocalDate.now(ZoneId.systemDefault()), errorsList, HttpStatus.NOT_FOUND.value()),
                new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BindingException.class)
    public ResponseEntity<ErrorResponseDTO> handleException(BindingException e) {
        List<String> errorsList = (CollectionUtils.isEmpty(e.getErrors())) ? new LinkedList<>() : e.getErrors();
        return new ResponseEntity<>(new ErrorResponseDTO(LocalDate.now(ZoneId.systemDefault()), errorsList, e.getCode()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<ErrorResponseDTO> handleException(InvalidInputException e) {
        List<String> errorsList = (CollectionUtils.isEmpty(e.getErrors())) ? new LinkedList<>() : e.getErrors();
        return new ResponseEntity<>(new ErrorResponseDTO(LocalDate.now(ZoneId.systemDefault()), errorsList, e.getCode()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponseDTO> handleException(Exception e) {
        logger.error(DEFAULT_ERROR_MESSAGE, e);
        String errorMessage = e.getMessage() != null ? e.getMessage() : DEFAULT_ERROR_MESSAGE;
        return new ResponseEntity<>(new ErrorResponseDTO(LocalDate.now(ZoneId.systemDefault()), Arrays.asList(errorMessage), DEFAULT_ERROR_CODE),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(FabrickInternalErrorException.class)
    public ResponseEntity<ErrorResponseDTO> handleException(FabrickInternalErrorException e) {
        List<String> errorsList =  new LinkedList<>();
        errorsList.add(e.getMessage());
        return new ResponseEntity<>(new ErrorResponseDTO(LocalDate.now(ZoneId.systemDefault()), errorsList, DEFAULT_ERROR_CODE),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
