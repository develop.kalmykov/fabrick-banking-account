package it.fabrick.bankaccount.controllers;

import it.fabrick.bankaccount.exceptions.BindingException;
import it.fabrick.bankaccount.exceptions.FabrickInternalErrorException;
import it.fabrick.bankaccount.exceptions.FabrickNotFoundExceptionException;
import it.fabrick.bankaccount.models.dto.fabrick.account.BankingAccountPayloadDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferRequestDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferResponseDTO;
import it.fabrick.bankaccount.services.IBankingAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/banking/accounts")
@RequiredArgsConstructor
public class BankingAccountController {
    private final IBankingAccountService service;
    @Autowired
    private ResourceBundleMessageSource errMessage;

    @GetMapping("/transactions")
    public ResponseEntity<List<BankingAccountPayloadDTO>> getAccountTransactionsList(@RequestParam(name = "dateFrom", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateFrom,
                                                                                     @RequestParam(name = "dateTo", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateTo) throws FabrickInternalErrorException {
        List<BankingAccountPayloadDTO> accountCreditList = service.getAccountTransactionsList(dateFrom, dateTo);
        return new ResponseEntity<>(accountCreditList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BankingAccountPayloadDTO> getAccountCredit(@PathVariable(name = "id") Long accountId) throws FabrickNotFoundExceptionException {
        BankingAccountPayloadDTO accountCredit = service.getAccountCredit(accountId);
        return new ResponseEntity<>(accountCredit, HttpStatus.OK);
    }

    @PostMapping("/{id}/money-transfer")
    public ResponseEntity<MoneyTransferResponseDTO> makeMoneyTransfer(@PathVariable(name = "id") Long accountId,
                                                                      @RequestBody @Valid MoneyTransferRequestDTO request,
                                                                      BindingResult bindingResult) throws FabrickInternalErrorException, BindingException {
        if (bindingResult.hasErrors()) {
            List<String> msgErr = new LinkedList<>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                msgErr.add(errMessage.getMessage(error, LocaleContextHolder.getLocale()));
            }
            log.error(String.valueOf(Arrays.asList(msgErr)));
            throw new BindingException(msgErr);
        }
        MoneyTransferResponseDTO moneyTransfer = service.makeMoneyTransfer(accountId, request);
        return new ResponseEntity<>(moneyTransfer, HttpStatus.CREATED);
    }
}
