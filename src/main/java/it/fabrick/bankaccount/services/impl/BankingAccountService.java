package it.fabrick.bankaccount.services.impl;

import it.fabrick.bankaccount.exceptions.FabrickInternalErrorException;
import it.fabrick.bankaccount.exceptions.FabrickNotFoundExceptionException;
import it.fabrick.bankaccount.models.dto.fabrick.account.BankingAccountPayloadDTO;
import it.fabrick.bankaccount.models.dto.fabrick.account.BankingAccountResponseDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferRequestDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferResponseDTO;
import it.fabrick.bankaccount.restclient.FabrickRestClient;
import it.fabrick.bankaccount.services.IBankingAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class BankingAccountService implements IBankingAccountService {
    private final FabrickRestClient restClient;

    @Value("${fabrick.auth-schema}")
    private String authSchema;

    @Value("${fabrick.api-key}")
    private String apiKey;

    @Value("${fabrick.time-zone}")
    private String timeZone;

    @Override
    public List<BankingAccountPayloadDTO> getAccountTransactionsList(LocalDate dateFrom, LocalDate dateTo) throws FabrickInternalErrorException {
        log.info("[START] BankingAccountService::getAccountCreditList");
        BankingAccountResponseDTO response;
        try {
            response = restClient.getAccountTransactionsList(authSchema, apiKey, dateFrom, dateTo).getBody();
        } catch (Exception e) {
            log.error("Error http remote call: " + e.getMessage());
            throw new FabrickInternalErrorException("Errore recupero lista transazioni");
        }

        if (response.getPayload() != null) {
            log.info("[END] BankingAccountService::getAccountCreditList");
            return response.getPayload().getList();
        }

        log.info("[END] BankingAccountService::getAccountCreditList. No element has been found");
        return new LinkedList<>();
    }

    @Override
    public BankingAccountPayloadDTO getAccountCredit(Long accountId) throws FabrickNotFoundExceptionException {
        log.info(String.format("[START] BankingAccountService::getAccountCredit with id: %d", accountId));
        BankingAccountResponseDTO response;
        try {
            response = restClient.getAccountCredit(authSchema, apiKey, accountId).getBody();
        } catch (Exception e) {
            log.error("Error http remote call: " + e.getMessage());
            throw new FabrickNotFoundExceptionException("Acconto non trovato");
        }
        log.info("[END] BankingAccountService::getAccountCredit");
        return response.getPayload();
    }

    @Override
    public MoneyTransferResponseDTO makeMoneyTransfer(Long accountId, MoneyTransferRequestDTO request) throws FabrickInternalErrorException {
        log.info(String.format("[START] BankingAccountService::makeMoneyTransfer with request: %s", request));
        MoneyTransferResponseDTO response;
        try {
            response = restClient.makeMoneyTransfer(authSchema, apiKey, timeZone, accountId, request).getBody();
        } catch (Exception e) {
            log.error("Error http remote call: " + e.getMessage());
            throw new FabrickInternalErrorException("Errore invio bonifico");
        }
        log.info("[END] BankingAccountService::makeMoneyTransfer");
        return response;
    }
}
