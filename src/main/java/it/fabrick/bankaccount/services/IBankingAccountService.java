package it.fabrick.bankaccount.services;

import it.fabrick.bankaccount.exceptions.FabrickInternalErrorException;
import it.fabrick.bankaccount.exceptions.FabrickNotFoundExceptionException;
import it.fabrick.bankaccount.models.dto.fabrick.account.BankingAccountPayloadDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferRequestDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferResponseDTO;

import java.time.LocalDate;
import java.util.List;

public interface IBankingAccountService {

    List<BankingAccountPayloadDTO> getAccountTransactionsList(LocalDate dateFrom, LocalDate dateTo) throws FabrickInternalErrorException;

    BankingAccountPayloadDTO getAccountCredit(Long accountId) throws FabrickNotFoundExceptionException;

    MoneyTransferResponseDTO makeMoneyTransfer(Long accountId, MoneyTransferRequestDTO request) throws FabrickInternalErrorException;
}
