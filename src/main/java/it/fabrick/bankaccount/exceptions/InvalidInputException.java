package it.fabrick.bankaccount.exceptions;

import lombok.Data;

import java.util.List;

@Data
public class InvalidInputException extends BaseException {
    public InvalidInputException(List<String> errors, int code) {
        super(errors, code);
    }

    public InvalidInputException(List<String> errors) {
        super(errors, 400);
    }
}
