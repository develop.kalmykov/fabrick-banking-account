package it.fabrick.bankaccount.exceptions;

public class FabrickNotFoundExceptionException extends RuntimeException {
    public FabrickNotFoundExceptionException(String error) {
        super(error);
    }
}