package it.fabrick.bankaccount.exceptions;

import lombok.Data;

import java.util.List;

@Data
public class BindingException extends BaseException {
    public BindingException(List<String> errors, int code) {
        super(errors, code);
    }

    public BindingException(List<String> errors) {
        super(errors, 400);
    }
}
