package it.fabrick.bankaccount.exceptions;

import lombok.Data;

import java.util.List;

@Data
public class BaseException extends Exception {
    private String error;
    private List<String> errors;
    private int code;

    public BaseException() {
        super();
    }

    public BaseException(List<String> errors) {
        this.errors = errors;
        this.code = -1;
    }

    public BaseException(String error) {
        this.error = error;
        this.code = -1;
    }

    public BaseException(String error, int code) {
        this.error = error;
        this.code = code;
    }

    public BaseException(List<String> errors, int code) {
        this.errors = errors;
        this.code = code;
    }
}
