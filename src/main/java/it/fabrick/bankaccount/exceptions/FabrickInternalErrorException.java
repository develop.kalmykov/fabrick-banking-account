package it.fabrick.bankaccount.exceptions;

public class FabrickInternalErrorException extends RuntimeException {
    public FabrickInternalErrorException(String error) {
        super(error);
    }
}
