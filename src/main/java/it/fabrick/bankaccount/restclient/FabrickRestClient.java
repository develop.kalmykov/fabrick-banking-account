package it.fabrick.bankaccount.restclient;

import it.fabrick.bankaccount.models.dto.fabrick.account.BankingAccountResponseDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferRequestDTO;
import it.fabrick.bankaccount.models.dto.fabrick.moneytransfer.MoneyTransferResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@FeignClient(name = "cabel-client", url = "${fabrick.base-url}")
public interface FabrickRestClient {
    @GetMapping(value = "/api/gbs/banking/v4.0/accounts")
    ResponseEntity<BankingAccountResponseDTO> getAccountTransactionsList(@RequestHeader("Auth-Schema") String authSchema,
                                                                         @RequestHeader("apiKey") String apiKey,
                                                                         @RequestParam(name = "fromAccountingDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateFrom,
                                                                         @RequestParam(name = "toAccountingDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateTo);

    @GetMapping(value = "/api/gbs/banking/v4.0/accounts/{accountId}")
    ResponseEntity<BankingAccountResponseDTO> getAccountCredit(@RequestHeader("Auth-Schema") String authSchema,
                                                               @RequestHeader("apiKey") String apiKey,
                                                               @PathVariable("accountId") Long accountId);

    @PostMapping(value = "/api/gbs/banking/v4.0/accounts/{accountId}/payments/money-transfers")
    ResponseEntity<MoneyTransferResponseDTO> makeMoneyTransfer(@RequestHeader("Auth-Schema") String authSchema,
                                                               @RequestHeader("apiKey") String apiKey,
                                                               @RequestHeader("X-Time-Zone") String timeZone,
                                                               @PathVariable("accountId") Long accountId,
                                                               @RequestBody MoneyTransferRequestDTO request);
}
