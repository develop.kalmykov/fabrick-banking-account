package it.fabrick.bankaccount.models.dto.fabrick.moneytransfer;

import it.fabrick.bankaccount.models.dto.fabrick.BaseResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTransferResponseDTO extends BaseResponseDTO {
    private MoneyTransferPayloadDTO payload;
}
