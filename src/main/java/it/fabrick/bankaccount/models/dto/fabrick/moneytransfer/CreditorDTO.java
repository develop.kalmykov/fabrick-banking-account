package it.fabrick.bankaccount.models.dto.fabrick.moneytransfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditorDTO {
    @NotEmpty(message = "{field.validation.creditor.name}")
    private String name;

    @Valid
    @NotNull(message = "{field.validation.creditor.account}")
    private AccountDTO account;
}
