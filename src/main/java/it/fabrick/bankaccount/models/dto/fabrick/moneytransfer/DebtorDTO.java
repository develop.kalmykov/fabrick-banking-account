package it.fabrick.bankaccount.models.dto.fabrick.moneytransfer;

import lombok.Data;

@Data
public class DebtorDTO extends CreditorDTO {
}
