package it.fabrick.bankaccount.models.dto.fabrick.account;


import it.fabrick.bankaccount.models.dto.fabrick.BaseResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankingAccountResponseDTO extends BaseResponseDTO {
    private BankingAccountPayloadDTO payload;
}


