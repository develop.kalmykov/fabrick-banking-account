package it.fabrick.bankaccount.models.dto.fabrick.moneytransfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTransferRequestDTO {
    @Valid
    @NotNull(message = "{field.validation.creditor}")
    private CreditorDTO creditor;

    @NotNull(message = "{field.validation.executionDate}")
    private LocalDate executionDate;

    @NotEmpty(message = "{field.validation.description}")
    private String description;

    @NotNull(message = "{field.validation.amount}")
    private BigDecimal amount;

    @NotEmpty(message = "{field.validation.currency}")
    private String currency;
}
