package it.fabrick.bankaccount.models.dto.fabrick.moneytransfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {
    @NotEmpty(message = "{field.validation.account.accountCode}")
    private String accountCode;
}
