package it.fabrick.bankaccount.models.dto.fabrick.moneytransfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTransferPayloadDTO {
    private String moneyTransferId;
    private String cro;
    private String trn;
    private String status;
    private String uri;
    private String direction;
    private CreditorDTO creditor;
    private DebtorDTO debtor;
}