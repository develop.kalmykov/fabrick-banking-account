package it.fabrick.bankaccount.models.dto.fabrick;

import lombok.Data;

import java.util.List;

@Data
public class BaseResponseDTO {
    private String status;
    private List<String> error;
}
